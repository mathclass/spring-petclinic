import semver
import git
import re

def get_latest_tag(repo):
    tags = [tag for tag in repo.tags if re.match(r'^v?\d+\.\d+\.\d+$', str(tag))]
    if tags:
        sorted_tags = sorted(tags, key=lambda t: semver.VersionInfo.parse(str(t).lstrip('v')), reverse=True)
        return sorted_tags[0]
    return None


repo = git.Repo('.')
latest_tag = get_latest_tag(repo)
if latest_tag:
    # Parse the latest version and increment the minor version
    current_version = semver.VersionInfo.parse(str(latest_tag).lstrip('v'))
    new_version = current_version.bump_minor()
else:
    new_version = semver.VersionInfo.parse("0.1.0")

new_tag = f'v{new_version}'
repo.create_tag(new_tag)
repo.remotes.origin.push(new_tag)

with open('VERSION', 'w') as version_file:
    version_file.write(f'{new_version}')
print(f'Created new cool mega tag: v{new_version}')

