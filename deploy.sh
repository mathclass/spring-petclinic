#!/bin/bash
sudo -i
source ~/env_vars.sh

if [ -f current_version.txt ]; then
    CURRENT_VERSION=$(cat current_version.txt)
    if [ "$(printf '%s\n' "$VERSION" "$CURRENT_VERSION" | sort -V | head -n1)" != "$VERSION" ]; then
        echo "Deploying new cool version $VERSION, removing old version $CURRENT_VERSION."
        sudo docker stop app
        sudo docker rm app
    else
        echo "Current version $CURRENT_VERSION is up to date or newer. No deployment needed."
        exit 0
    fi
else
    echo "No version file found. Deploying cool version $VERSION."
    touch current_version.txt
fi

sudo docker pull mivancevic/main:$VERSION
echo $VERSION > current_version.txt
sudo docker run -d --name app \
    -e GOOGLE_APPLICATION_CREDENTIALS=/app/service-account.json \
    -e SPRING_PROFILES_ACTIVE=mysql \
    -e DBNAME=$DB_NAME \
    -e DBUSER=$DB_USER \
    -e DBPW=$DB_PASS \
    -e DBICONNNAME=$DB_INSTANCE_CONNECTION_NAME \
    -v ~/gcloud-service-key.json:/app/service-account.json \
    -p 80:8080 mivancevic/main:$VERSION
